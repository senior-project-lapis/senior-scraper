Senior Scraper (scrapy & scrapyd)
====================

It is a Scrapy project with scrapyd for senior project CPE KMUTT 2021.

<!-- TOC -->

- [Senior Scraper (scrapy & scrapyd)](#senior-scraper-scrapy--scrapyd)
  - [You must know](#you-must-know)
  - [Installing and Running](#installing-and-running)
  - [Curl command](#curl-command)
  - [Example of scraped data](#example-of-scraped-data)
  - [Project Structure](#project-structure)

<!-- /TOC -->


## You must know
- [Scrapy](https://docs.scrapy.org/en/latest/)
- [Scrapyd](https://scrapyd.readthedocs.io/en/stable/)


## Installing and Running

1. Clone this repo:

        $ git clone git@github.com:stummjr/scrapy_price_monitor.git

2. Enter the folder and install the project dependencies:

        $ cd senior-scraper
        $ pip install -r requirements.txt

3. Run server:

        $ scrapyd

    This should show terminal like this:
    
        . . .
        2021-10-20T22:54:06+0700 [-] Site starting on 6800
        2021-10-20T22:54:06+0700 [twisted.web.server.Site#info] Starting factory <twisted.web.server.Site object at 0x00000232DEF9C2B0>
        2021-10-20T22:54:06+0700 [Launcher] Scrapyd 1.2.1 started: max_proc=32, runner='scrapyd.runner'

4. Open new terminal to test request:

        $ curl http://localhost:6800/schedule.json -d project=default -d spider=extracturlbot -d force=y -d start=0 -d end=200

5. See the result on [http://localhost:6800](http://localhost:6800) (See in `Jobs` menu)

## Curl command

Scrape only review url 

        $ curl http://localhost:6800/schedule.json -d project=default -d spider=getcompanybot

Extract review url with condition

        $ curl http://localhost:6800/schedule.json -d project=default -d spider=extracturlbot -d force=y -d start=0 -d end=100

Scrape review data of each company

        $ curl http://localhost:6800/schedule.json -d project=default -d spider=reviewbot -d force=y

Parameter:

 - `-d project=?` : select project to run
 - `-d spider=?` : select bot to run
 - `-d force=?` : if force is 'y', it will delete and create new file
 - `-d start=?` : first index of company review url
 - `-d end=?` : last index of company review url


## Example of scraped data

getcompanybot (Get company bot)

    company,url
    company_name_1,https://company1/reviews
    company_name_2,https://company2/reviews
    . . .

extracturlbot (Extract url bot)

    https://company1/reviews?fcountry=US&lang=en
    https://company1/reviews?fcountry=CA&lang=en
    https://company2/reviews?fcountry=US&lang=en
    . . .

reviewbot (Review bot)

    {
      "reviews": [
        {
          "company": "A",
          "title": "Overall it was a very productive job",
          "score": 2.0,
          "author_position": "Assistant Manager",
          "content": "My coworkers were always encouraging.",
          "pro": "good food",
          "con": "early working"
        }
      ]
    }


## Project Structure

    └── seniorScraper
        ├── public                            - Store file that scraped.
        ├── seniorScraper                     - * APIs Controller.
        |   ├── spiders                       - Store bot file.
        |   |   ├── __init__.py               - Init file for python.
        |   |   ├── extracturlbot.py          - Bot for extract url.
        |   |   ├── getcompanybot.py          - Bot for getting company.
        |   |   └── reviewbot.py              - Bot for getting review.
        |   ├── __init__.py                   - Init file for python.
        |   ├── items.py                      - Manage item field (like model).
        |   ├── middlewares.py                - Middlewares.
        |   ├── pipelines.py                  - Pipeline process for data.
        |   └── settings.py                   - Setting for bot and project.
        ├── Dockerfile                        - Dockerfile to setting container.
        ├── requirements.txt                  - All python package that must use.
        └── scrapy.cfg                        - Config for scrapy project.
