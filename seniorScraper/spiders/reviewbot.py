from cmath import log
import logging
from time import daylight, sleep
import scrapy
import random
import pandas as pd
import datetime
import regex as re
from scrapy.exceptions import CloseSpider

class ReviewbotSpider(scrapy.Spider):
    name = 'reviewbot'

    custom_settings = {
        'ITEM_PIPELINES': {
            'seniorScraper.pipelines.ReviewToCsvPipeline': 300,
        }
    }

    def start_requests(self):
        count = 0
        raw_company_df = pd.read_csv('public/Company_review_url.csv', names=['url'])
        company_review = raw_company_df['url'].tolist()
        for url in company_review:
            count += 1
            logging.debug("Count: "+ str(count))
            sleep(1)
            yield scrapy.Request(url)

    def parse(self, response):
        if response.status == 429:      
            print("TOO MANY REQUEST")
            sleep(60)

        scrape_next_page = True
        if response.xpath('.//*[@data-testid="emptyPageText"]').get() != None:
            return

        company = response.xpath(
            './/div[@class="css-17x766f e1wnkr790"]/text()').get()
        if company == None:
            company = response.xpath(
                './/div[@class="css-1e5qoy2 e37uo190"]/div/div[@itemprop="name"]/text()').get()

        for review in response.xpath('.//*[@data-tn-section="reviews"]'):
            raw_date = review.xpath(
                './/span[@itemprop="author"]').get()
            date_str = re.search(r'\w+ \d{1,2}, \d{4}', str(raw_date))
            date = datetime.datetime.strptime(
                str(date_str.group()), "%B %d, %Y")

            # Query date condition limit last 1 year
            date_now = datetime.date.today()
            years_to_add = date_now.year - 1
            limit = date_now.replace(year=years_to_add).strftime('%Y-%m-%d')
            limit_date = datetime.datetime.strptime(
                str(limit), "%Y-%m-%d")
            if date < limit_date:
                scrape_next_page = False
                break

            title = review.xpath(
                './/h2[@data-testid="title"]/a/span/span/span/text()').get()
            score = review.xpath(
                './/div[@itemprop="reviewRating"]/button/text()').get()
            author_position = review.xpath(
                './/span[@itemprop="author"]/a/text()').get()
            content = review.xpath(
                './/div[@data-tn-component="reviewDescription"]/span[@itemprop="reviewBody"]/span/span/span/text()').get()
            pro = review.xpath(
                './/div/h2[@class="css-6pbru9 e1tiznh50"]/following-sibling::div/span/span/text()').get()
            con = review.xpath(
                './/div/h2[@class="css-cvf89l e1tiznh50"]/following-sibling::div/span/span/text()').get()
            item = {
                'companyName': company,
                'title': title,
                'score': float(score),
                'authorPosition': author_position,
                'content': content,
                'pro': pro,
                'con': con,
                'createAt': datetime.datetime.strptime(str(date), '%Y-%m-%d %H:%M:%S'),
                'updateAt': datetime.datetime.strptime(str(date), '%Y-%m-%d %H:%M:%S'),
                'topic': None,
                'sentiment': None
            }
            yield item

        if scrape_next_page:
            next_page = response.xpath(
                '//a[@data-tn-element="next-page"]/@href').get()

            if next_page:
                yield scrapy.Request(response.urljoin(next_page))
