from time import sleep
import scrapy


class GetCompanybotSpider(scrapy.Spider):
    name = 'getcompanybot'

    custom_settings = {
        'ITEM_PIPELINES': {
            'seniorScraper.pipelines.CompanyReviewToCsvPipeline': 300,
        }
    }

    def start_requests(self):
        categories = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                      'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'other']
        base_url = 'https://www.indeed.com/companies/browse-companies/'

        for category in categories:
            category_url = base_url + category
            # sleep(0.2)
            yield scrapy.Request(category_url)

    def parse(self, response):

        conpany_name_list = response.xpath(
            './/h3[@class="css-1gx042g-Text e1wnkr790"]/text()').extract()
        review_path_url_list = response.xpath(
            './/ul[@data-cy="companies-list"]/li/ul[@class="css-1udsd81-Box eu4oa1w0"]/li[2]/a[@class="css-e5hjtw-Link emf9s7v0"]/@href').extract()

        for company in zip(conpany_name_list, review_path_url_list):
            item = {
                'company': company[0], # company name
                'url': 'https://www.indeed.com' + company[1] # review path url
            }
            yield item

        # get next page and add to queue
        next_page = response.xpath(
            '//ul[@data-cy="numeric-pagination"]/li[@class="css-ygcrdr-Box eu4oa1w0"]/a[@class="css-h7qbka-Link emf9s7v0"]/../following-sibling::li/a[@class="css-yxc5s0-Link emf9s7v0"]/@href').get()
        if next_page:
            yield scrapy.Request(response.urljoin(next_page))
