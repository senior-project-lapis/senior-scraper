from time import sleep
import scrapy
import random
import json
import pandas as pd
import requests
from bs4 import BeautifulSoup


class ExtractUrlBotSpider(scrapy.Spider):
    name = 'extracturlbot'

    custom_settings = {
        'ITEM_PIPELINES': {
            'seniorScraper.pipelines.ExtractUrlToCsvPipeline': 300,
        }
    }

    def __init__(self, *args, **kwargs):
        super(ExtractUrlBotSpider, self).__init__(*args, **kwargs)

    def start_requests(self):
        company_df = pd.read_csv('public/Company_review_dataframe.csv')
        company_review_list = company_df['url'].tolist()
        country_select = ['US', 'UK', 'SG', 'CA']
        lang_select = 'en'

        for base_url in company_review_list[int(self.start):int(self.end)]:
            for country in country_select:
                url = base_url + '?fcountry=' + country + '&lang=' + lang_select
                yield scrapy.Request(url)

    def parse(self, response):
        is_empty_page = response.xpath(
            './/div[@data-testid="emptyPageText"]').get()
        if not is_empty_page:
            item = {'url': response.url}
            yield item

    def close(self, reason):
        start_time = self.crawler.stats.get_value('start_time')
        finish_time = self.crawler.stats.get_value('finish_time')
        print("Total run time: ", finish_time-start_time)
