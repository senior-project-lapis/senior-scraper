# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import json
import os
from datetime import date
import logging
import pymongo


class CompanyReviewToCsvPipeline:
    def open_spider(self, spider):
        path = './public/Company_review_dataframe.csv'
        if os.path.exists(path):
            os.remove(path)
        self.file = open(path, 'w+')
        self.file.write('company,url\n')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        self.file.write('"' + str(item['company']) +
                        '","' + str(item['url']) + '"\n')
        return item


class ExtractUrlToCsvPipeline:
    def open_spider(self, spider):
        path = './public/Company_review_url.csv'
        if spider.force == 'y':
            if os.path.exists(path):
                os.remove(path)

        self.file = open(path, 'a+')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        self.file.write('"' + str(item['url']) + '"\n')
        return item


class ReviewToCsvPipeline:
    collection_name = 'reviews'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE')
        )

    def open_spider(self, spider):
        # mongo
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

        # path = './public/reviews.csv'
        # if os.path.exists(path):
        #     os.remove(path)
        # self.file = open(path, 'w+')
        # self.file.write(
        #     'company,title,score,author_position,content,pro,con,create_date\n')

    def close_spider(self, spider):
        # mongodb
        self.client.close()

        # self.file.close()
        # print(date.today(), " [-] Write file finished")

    def process_item(self, item, spider):
        # mongodb
        new_company_obj = self.db['companies'].find_one({ "name": item['companyName'] }, {'_id': 1})
        dupplicate_review = self.db[self.collection_name].find_one({"score": item["score"], "title": item['title'], "content": item['content'], "pro": item["pro"], "con": item["con"] }, {'_id': 1})
        if new_company_obj and not dupplicate_review:
            item['companyId'] = new_company_obj['_id']
            item.pop('companyName', None)
            self.db[self.collection_name].insert_one(dict(item))
            logging.debug("Review added to MongoDB")

            # self.file.write('"' + str(item['title']).replace('\n', '').replace('\r', '').replace('"', '').replace(',', '') + '",' +
            #                 str(item['score']).replace('\n', '').replace('\r', '').replace('"', '').replace(',', '') + ',' +
            #                 '"' + str(item['authorPosition']).replace('\n', '').replace('\r', '').replace('"', '').replace(',', '') + '",' +
            #                 '"' + str(item['content']).replace('\n', '').replace('\r', '').replace('"', '').replace(',', '') + '",' +
            #                 '"' + str(item['pro']).replace('\n', '').replace('\r', '').replace('"', '').replace(',', '') + '",' +
            #                 '"' + str(item['con']).replace('\n', '').replace('\r', '').replace('"', '').replace(',', '') + '",' +
            #                 '"' + str(item['createAt']) + '",' +
            #                 '"' + str(item['updateAt']) + '",' +
            #                 '"' + str(item['companyId']) + '"' +
            #                 '\n')
        return item

class JobPipeline(object):

    collection_name = 'jobs'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        new_company_obj = self.db['companies'].find_one({ "sourceId": item['companyId'] }, {'_id': 1})
        if new_company_obj:
            item['companyId'] = new_company_obj['_id']
        self.db[self.collection_name].insert_one(dict(item))
        logging.debug("Job added to MongoDB")
        return item

class MuseCompanyPipeline:
    collection_name = 'companies'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE')
        )
    
    def open_spider(self, spider):
        # self.file = open('./public/result.json', 'w')
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    
    def process_item(self, item, spider):       
        
        try:
            if self.db[self.collection_name].find_one({"sourceId": item['sourceId']}):
                logging.debug("Failed to add company [Duplicate key found]") 
                # line = json.dumps(dict(item)) + ",\n"  #
                # self.file.write(line)    #
                return item
            else:
                self.db[self.collection_name].insert_one(dict(item))
                logging.debug("Company added to MongoDB") 
        except:
            logging.debug("Failed to add company [Error]")
            
        return item
 
 
    def close_spider(self, spider):
        # self.file.close()
        self.client.close()