import scraper_helper as sh
# Scrapy settings for seniorScraper project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

# [Variable]

BOT_NAME = 'seniorScraper'

SPIDER_MODULES = ['seniorScraper.spiders']
NEWSPIDER_MODULE = 'seniorScraper.spiders'
TELNETCONSOLE_PORT = None

MONGO_URI = 'mongodb+srv://dive:XjX7gp7OExCp5yrb@senior-pro.mtbns.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
MONGO_DATABASE = 'senior-pro'

API_KEY = '71044d808e8ac641f54ac6832c227d63606e6e20439089321a4834486ebc227e'

# Desired file format
# FEED_FORMAT = "json"

# Name of the file where data extracted is stored
# FEED_URI = "company_url.json"

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'seniorScraper (+http://www.yourdomain.com)'

# PROXY_POOL_ENABLED = True

# Obey robots.txt rules
ROBOTSTXT_OBEY = True

# Configure maximum concurrent requests performed by Scrapy (default: 16)
REACTOR_THREADPOOL_MAXSIZE = 128
CONCURRENT_REQUESTS = 256

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 0
DOWNLOAD_TIMEOUT = 30
RANDOMIZE_DOWNLOAD_DELAY = True

# The download delay setting will honor only one of:
CONCURRENT_REQUESTS_PER_DOMAIN = 256
CONCURRENT_REQUESTS_PER_IP = 256

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
AUTOTHROTTLE_ENABLED = True
# The initial download delay
AUTOTHROTTLE_START_DELAY = 1
# The maximum download delay to be set in case of high latencies
AUTOTHROTTLE_MAX_DELAY = 0.25
# The average number of requests Scrapy should be sending in parallel to
# each remote server
AUTOTHROTTLE_TARGET_CONCURRENCY = 128
# Enable showing throttling stats for every response received:
AUTOTHROTTLE_DEBUG = False

# Retry
RETRY_ENABLED = True
RETRY_TIMES = 1
RETRY_HTTP_CODES = [500, 502, 503, 504, 400, 401,
                    403, 404, 405, 406, 407, 408, 409, 410, 429] # 429
# ETRY_HTTP_CODES = [429] # 429

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
    'scrapy.spidermiddlewares.referer.RefererMiddleware': 80,
    # 'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
    # 'seniorScraper.middlewares.ProxyMiddleware': 432,
    # 'seniorScraper.middlewares.TooManyRequestsRetryMiddleware': 543,
    'scrapy_fake_useragent.middleware.RandomUserAgentMiddleware': 120,
    'scrapy.downloadermiddlewares.cookies.CookiesMiddleware': 130,
    'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
    'scrapy.downloadermiddlewares.redirect.RedirectMiddleware': 900,
}

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
# DEFAULT_REQUEST_HEADERS = sh.get_dict(
#   '''
#   accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
#   accept-encoding: gzip, deflate, br
#   accept-language: en,th;q=0.9,th-TH;q=0.8
#   cache-control: max-age=0
#   cookie: CTK=1fbbaolm3npjn801; RF="TFTzyBUJoNr6YttPP3kyivpZ6-9J49o-Uk3iY6QNQqKE2fh7FyVgtWz84sUp6S4jorZpYKhh9u4="; _gcl_au=1.1.1477220799.1627099258; _ga=GA1.2.297066844.1627099259; indeed_rcc=CTK; __ssid=518b60aa24c5c3a3a496ce63fa57f05; OptanonAlertBoxClosed=2021-08-11T12:15:13.191Z; pjps=1; SESSION_START_TIME=1630311540799; SESSION_ID=1feb27q1vqeit800; SESSION_END_TIME=1630312243370; RQ="q=Trend+Charts&l=&ts=1630312302985:q=Trend&l=&ts=1630312291898:q=Job+Trends&l=&ts=1630312268976:q=API+Open+Source+Developer&l=&ts=1629985273710"; CO=TH; optimizelyEndUserId=oeu1630314109498r0.49737933942300505; _mkto_trk=id:699-SXJ-715&token:_mch-indeed.com-1630314113142-82350; LOCALE=th; IRF=7YXfKDzcx1JT-QT4QnjgSMq9HgVJbSc3YBhmbuHWaKZo1hClRZrIhg==; OptanonConsent=isIABGlobal=false&datestamp=Wed+Sep+15+2021+16%3A29%3A57+GMT%2B0700+(Indochina+Time)&version=6.13.0&hosts=&consentId=d71b8b4a-749b-4f7a-b3b0-a655327c733d&interactionCount=1&landingPath=NotLandingPage&groups=C0001%3A1%2CC0002%3A1%2CC0003%3A1%2CC0004%3A1&geolocation=%3B&AwaitingReconsent=false; LV="LA=1633418703:LV=1629983247:CV=1633418703:TS=1627109976"; UD="LA=1633418703:LV=1629983247:CV=1633418703:TS=1627109976"; CMP_VISITED=1; temp_locale=en_TH; LC=co=US&hl=en; g_state={"i_p":1634185900293,"i_l":3}; CO=TH; LOCALE=en; INDEED_CSRF_TOKEN=x28WkjOFaj3nimwJIrutt9cCjkjO3xmd; _gid=GA1.2.1225731410.1634113584; SURF=V2LLK3nNdMDUrg1oPAnCn4M7j9xqgXGy; CSRF=Wu3xbxEFeejTSJr3jZLxor3M4kPWzx8A; bvcmpgn=ac2323_aur_g1; indeed_rcc="cmppmeta:CTK"; cmppmeta="eNoBSAC3/+9UKmijMAmhC0s9lFpGzC2pBLISPRwtg07EB8nEHqZqcT+XvvHOfIzAfUhD/BmzDPgmOUoUVz3v/PRvYGTPWfwxISDHDnlnVVxeH8Q="; _gali=login-submit-button; _gat=1; SOCK="sApPfR5i78Y0d_1m1fWFyct4fcs="; SHOE="m4RIDYpHFZ42avG878wzGVywFohtFNB3KTp26BrojMAQdzh-X9Nzp-CyesmRwaf6-LjQn89ofNdWhZTUKI6-MNCUolRk4rIJXwHG57z7MX4DgNccpSvS7tgSiznHXvJomLsOYK-Diq1n"; JSESSIONID=7CFE4506097A58C8342C748954A555DB; PPID=eyJraWQiOiJjOWJjYWFmMC1kZDFlLTQ4YmQtYThlYS1iYmMyNDAyMzk4NDQiLCJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiJ9.eyJzdWIiOiIwZTJiZTU5NzdhNWUwYWNiIiwiYXVkIjoiYzFhYjhmMDRmIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJjcmVhdGVkIjoxNjMzNDMxMjU3MDAwLCJyZW1fbWUiOnRydWUsImlzcyI6Imh0dHBzOlwvXC9zZWN1cmUuaW5kZWVkLmNvbSIsImV4cCI6MTYzNDExNjcyOSwiaWF0IjoxNjM0MTE0OTI5LCJsb2dfdHMiOjE2MzQxMTQ5MjcsImVtYWlsIjoia2FzaWRpdC5kaXZlQG1haWwua211dHQuYWMudGgifQ.FjnJ2gRPRiIcX3UfrTajSuBrbrgbr5YzHfEjAPV6CoWndLp4f1nmwiTbyvKVHxv8XZvbgnVg1saLTAs2M8AR1A
#   referer: https://secure.indeed.com/account/login
#   sec-ch-ua: "Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"
#   sec-ch-ua-mobile: ?0
#   sec-ch-ua-platform: "Windows"
#   sec-fetch-dest: document
#   sec-fetch-mode: navigate
#   sec-fetch-site: same-site
#   sec-fetch-user: ?1
#   upgrade-insecure-requests: 1
#   user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36
#   '''
# )

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
# SPIDER_MIDDLEWARES = {
#    'seniorScraper.middlewares.SeniorscraperSpiderMiddleware': 543,
# }

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
# EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
# }

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'seniorScraper.pipelines.ExtractUrlToCsvPipeline': 300,
    'seniorScraper.pipelines.CompanyReviewToCsvPipeline': 300,
    'seniorScraper.pipelines.ReviewToCsvPipeline': 300,
    'seniorScraper.pipelines.JobPipeline': 300, 
    'seniorScraper.pipelines.MuseCompanyPipeline': 300,
}

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
